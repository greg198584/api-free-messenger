# API-FREE-MESSENGER

Ce projet utilise Gows, un framework pour créer des API.

[Visitez le dépôt Gows sur GitLab](https://gitlab.com/greg198584/gows)

## Prérequis

Avant de lancer l'API, créez un dossier `jwt` :

```bash
mkdir jwt
```

## Démarrage


### Exécution en local

Pour démarrer l'API en mode développement, exécutez la commande suivante :

```bash
CFG_PATH=./config/ ENV=DEV DB_ACTIF="true" go run main.go
```