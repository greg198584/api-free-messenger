package main

import (
	"gitlab.com/greg198584/api-free-messenger/controller"
	"gitlab.com/greg198584/gows/logger"
	"gitlab.com/greg198584/gows/middlewares"
	"gitlab.com/greg198584/gows/module_controller"
	"gitlab.com/greg198584/gows/router"
	"os"
)

func main() {
	err := router.New(&controller.MainController{}).
		AddMiddleware("Jwt", middlewares.Jwt).
		Use(&module_controller.MainController{}).
		Listen()
	if err != nil {
		logger.GetLog("main_exemple").Error(err.Error())
		os.Exit(1)
	}
}
