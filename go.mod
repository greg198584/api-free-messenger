module gitlab.com/greg198584/api-free-messenger

go 1.18

require gitlab.com/greg198584/gows v1.1.0

require (
	github.com/getsentry/sentry-go v0.16.0 // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/twinj/uuid v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20221012134737-56aed061732a // indirect
	golang.org/x/sys v0.0.0-20220928140112-f11e5e49a4ec // indirect
	golang.org/x/text v0.3.7 // indirect
)
