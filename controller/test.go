package controller

import "net/http"

type TestData struct {
	Test1    string      `json:"test1"`
	Context1 interface{} `json:"context1"`
}

type Test struct {
	*MainController
}

func (mc *MainController) Test() *Test {
	return new(Test)
}

func (t *Test) ControllerTest() (err error) {
	t.JSON(http.StatusOK, "API OK")
	return
}
